const path = require('path');
const express = require('express');
const hbs = require('hbs');
const bodyParser = require('body-parser');
const app = express();

const conn = require('./db_config');

conn.connect((err) => {
    if (err) throw err;
    console.log('Mysql Connected...');
});


app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');
app.use(bodyParser.json());
app.use(
    bodyParser.urlencoded({
        extended: false
    })
)
app.use('/', express.static(__dirname + '/public'));

app.get('/', (req, res) => {
    let sql = "SELECT * FROM customer_bank";
    let query = conn.query(sql, (err, results) => {
        if (err) throw err;
        res.render('customer_bank', {
            results: results
        });
    });
});

app.post('/save', (req, res) => {
    let data = {
        nama: req.body.nama,
        alamat: req.body.alamat,
        kode_pos: req.body.kode_pos,
        no_hp: req.body.no_hp,
        email: req.body.email,
    };
    let sql = "INSERT INTO customer_bank SET ?";
    let query = conn.query(sql, data, (err, results) => {
        if (err) throw err;
        res.redirect('/');
    });
});

app.post('/update', (req, res) => {
    let sql = "UPDATE customer_bank SET nama='" + req.body.nama + "', alamat='" + req.body.alamat + "', kode_pos='" + req.body.kode_pos +
        "', no_hp='" + req.body.no_hp + "', email='" + req.body.email + "' WHERE cust_id=" + req.body.id;
    let query = conn.query(sql, (err, results) => {
        if (err) throw err;
        res.redirect('/');
    });
});

app.post('/delete', (req, res) => {
    let sql = "DELETE FROM customer_bank WHERE cust_id=" + req.body.cust_id + "";
    let query = conn.query(sql, (err, results) => {
        if (err) throw err;
        res.redirect('/');
    });
});

app.listen(8000, () => {
    console.log('Server is running at port 8000');
});